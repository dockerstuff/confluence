# README #

This is the Repository for the Confluence-Server Image.

### What is this repository for? ###
Within this Repo you will only find the Dockerfile and the pipeline configuration to build the container.


### How do I get set up? ###
To use this image interactive just run:
docker run -it felixkazuyade/java-base:latest

This will return a bash

### Contribution guidelines ###

This Repo is Creative Commons non Commercial - You can contribute by forking and using pull requests. The team will review them asap.



